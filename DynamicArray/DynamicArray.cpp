#include <iostream>
#include <string>

template<typename T>
bool defaultComparator(const T& a, const T& b) {
    return a < b;
}
std::string intToString(int data) {
    return std::to_string(data);
}
template <typename T> class DynamicArray {
private:
    int size;
    int maxSize = 1;
    T* array;
    double magFactor = 2.0;
public:
    void AddItemAsLast(T data) {
        T* tempData = new T();
        *tempData = data;
        AddItemAsLast(tempData);
    }
    void AddItemAsLast(T* data) {
        if (size == 0) {
            array = new T[maxSize];
            array[0] = *data;
            size = size + 1;
        }
        else if (size + 1 > maxSize) {
            T* tempArray;
            maxSize = maxSize * magFactor;
            tempArray = new T[maxSize];
            for (int i = 0; i <= size; i++) {
                tempArray[i] = array[i];
            }
            tempArray[size] = *data;
            size = size + 1;
            delete(array);
            array = tempArray;
        }
        else {
            array[size] = *data;
            size = size + 1;
        }
    }
    T GetElement(int index) {
        if (index < size && index > -1) {
            return array[index];
        }
        else {
            return -1;
        }
    }
    bool SetElement(int index, T newData) {
        if (index < size && index > -1) {
            array[index] = newData;
            return true;
        }
        else return false;
    }
    void Clear() {
        delete[] array;
        size = 0;
        maxSize = 1;
        magFactor = 2.0;
    }
    void Sort(bool (*comparator)(const T&, const T&)) {
        for (int i = 0; i < size - 1; ++i) {
            for (int j = 0; j < size - i - 1; ++j) {
                if (comparator(array[j + 1], array[j])) {
                    std::swap(array[j], array[j + 1]);
                }
            }
        }
    }
    std::string ToString() {
        return "Array has " + std::to_string(size) + " item(s).";
    }
    std::string ToString(std::string(*DataToString)(T)) {
        std::string finalText = "Array has " + std::to_string(size) + " item(s).";
        finalText += "\nFirst item: " + DataToString(array[0]);
        if (size > 1) finalText += "\nLast item: " + DataToString(array[size-1]);

        return finalText;
    }
};

int main()
{
    DynamicArray<int> dynamicArray;
    dynamicArray.AddItemAsLast(20);

    dynamicArray.AddItemAsLast(33);
    dynamicArray.AddItemAsLast(5);
    dynamicArray.AddItemAsLast(1);

    int asd = dynamicArray.GetElement(1);
    dynamicArray.Sort(defaultComparator);
    std::cout << dynamicArray.ToString() << std::endl;
    std::cout << dynamicArray.ToString(intToString) << std::endl;
    std::cout << dynamicArray.GetElement(2) << std::endl;
}

